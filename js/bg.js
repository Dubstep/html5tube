  chrome.webRequest.onBeforeRequest.addListener(
  function(details) { return {cancel: true}; },
  {urls: ["http://s.ytimg.com/yt/jsbin/html5player-*.js", "http://s.ytimg.com/yt/swfbin/*"]},
  ["blocking"]
  );

chrome.extension.onRequest.addListener(
  function(request,sender, sendResponse) {
    if (request.quality){
      var quality = request.quality;
      localStorage.setItem("quality",quality);
      sendResponse({"qualupdate": '"'+quality+'"'});
    }
    if (request.onload || request.inject){
      var quality = localStorage.getItem("quality");
      sendResponse({"quality": quality});
    }
    if (request.action){
      chrome.pageAction.show(sender.tab.id);
    }
    });